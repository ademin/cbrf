<?
class CBRF
{
    public       $currencyCodes = array();
    public       $error = '';
    protected    $soap;
    const        link = "http://www.cbr.ru/DailyInfoWebServ/DailyInfo.asmx?WSDL";
    const        cacheDir = 'cached';

    /** ------------------------------------------------------------------------
        Constructor
     */
    public function __construct()
    {
        $this->soap = new SoapClient( self::link );
        $this->getCurrencyCodes();
    }

    /** ------------------------------------------------------------------------
        Returns xml with soap responce from cbr.ru server
        $fromDate - 2013-02-01
        $ToDate   - 2013-02-29
        $currency - R01235/R0001...
     */
    protected function getRemoteXML( $fromDate, $toDate, $currency )
    {
        $params['FromDate']   = $fromDate . 'T00:00:00';
        $params['ToDate']     = $toDate   . 'T00:00:00';
        $params['ValutaCode'] = $currency;
        $params['On_date']   =  $fromDate . 'T00:00:00';
        try {
            //$response = $this->soap->GetCursOnDateXML( $params );
            $response = $this->soap->GetCursDynamicXML( $params );
        } catch ( Exception $e) {
            die( 'Error ocurred. Incorrect date?' );
        }

        return  $response->GetCursDynamicXMLResult->any;
    }
    
    /** ------------------------------------------------------------------------
        Returns xml with soap responce
        $fromDate - 2013-02-01
        $ToDate   - 2013-02-29
        $currency - R01235/R0001...
     */
    protected function getXML( $fromDate, $toDate, $currency )
    {
        $cacheFile = getcwd() . '/' .  self::cacheDir . '/' . $currency . '_' . $fromDate . '_' . date('YmdH') . '.xml';

        if ( !file_exists( $cacheFile ) || date( 'm', strtotime( $fromDate ) ) == date('m') ){
            $result = $this->getRemoteXML( $fromDate, $toDate, $currency );
            if( !@file_put_contents( $cacheFile, $result ) ){
                $this->createCacheDir( $cacheFile );
                file_put_contents( $cacheFile, $result );
            }
            return $result;
        } else {
            return file_get_contents( $cacheFile );
        }
    }

    /** ------------------------------------------------------------------------
        Returns currency rate
        $currency  - R01235/R0001...
        $month     - 1..12
        $year      - 2013
     */
    public function getRate( $month, $year, $currency )
    {
        $fromDate = date('Y-m-d', mktime( 0, 0, 0, $month, 1, $year ) );
        $toDate   = date('Y-m-d', mktime( 0, 0, 0, $month + 1, 0, $year ) );
        //$res = $this->getXML( $fromDate, $toDate, $currency );
        $xml = simplexml_load_string( $this->getXML( $fromDate, $toDate, $currency ) );
        $xPath = "/ValuteData/ValuteCursDynamic";
        $allDays = $xml->xpath( $xPath );
        $days = array();
        foreach($allDays as $day){
            $date = date( 'Y-m-d', strtotime($day->CursDate) );
            $days[] = array( 'date' => $date, 'value' => (float)$day->Vcurs / (float)$day->Vnom );
        }
        
        if ( !count( $days ) ){
            $this->error = 'There are no data for selected period';
            return false;
        }
        return $days;
    }

    /** ------------------------------------------------------------------------
        Fills currency array according data for today
     */
    protected function getCurrencyCodes()
    {
        $cacheCurFile = getcwd() . '/' .  self::cacheDir . '/' . 'currency.xml';

        if ( !file_exists( $cacheCurFile ) ){
            $response = $this->soap->EnumValutesXML( array('Seld' => False ) );
            $result = $response->EnumValutesXMLResult->any;
            if( !@file_put_contents( $cacheCurFile, $result ) ){
                $this->createCacheDir( $cacheCurFile );
                file_put_contents( $cacheCurFile, $result );
            }
        } else {
            $result = file_get_contents( $cacheCurFile );
        }
        $xml = simplexml_load_string( $result );
        $xPath = "/ValuteData/EnumValutes";
        $allCurrencies = $xml->xpath( $xPath );
        foreach( $allCurrencies as $currency ){
            $code = trim( $currency->Vcode );
            $this->currencyCodes[$code] = $currency->Vname;
        }
    }

    /** ------------------------------------------------------------------------
        Creates the cache dir.
        Rets true if path was really created.
     */
    protected function createCacheDir( $fname )
    {
        $dir = dirname( $fname );
        if ( !file_exists( $dir ) ){
            if ( mkdir( $dir, 0777, true ) ){
                return ( true );
            }
        }
        return ( false );
    }
}