<?
    require_once 'class.CBRF.php';

    $currency = isset( $_POST['currency'] ) ? $_POST['currency'] : 'USD';
    $month    = isset( $_POST['month']    ) ? $_POST['month']    : date( 'm' );
    $year     = isset( $_POST['year']     ) ? $_POST['year']     : date( 'Y' );

    for( $x = 1; $x < 13; $x++ ){
        $months[$x] = date('F', mktime( 0, 0, 0, $x, 1 ) );
    }
    $years = range( 1998, date( 'Y' ) );

    $cb = new CBRF();
    $currencies = $cb->currencyCodes;
    
    $json_str = '';
    if ( !empty( $_POST ) ){
        $result   = $cb->getRate( $month, $year, $currency );
        $json_str = json_encode( $result );
    }
?>
<!DOCTYPE html>
<html>
<head>
  <title>Currency rate</title>
  <script src="amcharts/amcharts.js" type="text/javascript"></script>
  <script src="amcharts/serial.js" type="text/javascript"></script>
<style>
#chartdiv {
  width: 1200px;
  height: 300px;
}
table{ border-collapse: collapse; width:200px }
td{ padding-right: 10px; }
tr{ border-bottom:1px dashed #bbbbbb; }
</style>
</head>
<body>
<script type="text/javascript">
var chart = AmCharts.makeChart("chartdiv", {
    "type": "serial",
    "theme": "none",

    "dataProvider": <?=$json_str?>,
    "valueAxes": [{
        "axisAlpha": 0,
        "dashLength": 4,
        "position": "left"
    }],
    "graphs": [{
        "bulletSize": 14,
        "bullet": "round",
        "valueField": "value"
    }],
    "marginTop": 20,
    "marginRight": 20,
    "marginLeft": 40,
    "marginBottom": 20,
    "chartCursor": {graphBulletSize:1.5},
    "autoMargins": false,
    "dataDateFormat": "YYYY-MM-DD",
    "categoryField": "date",
    "categoryAxis": {
        "parseDates": true,
        "axisAlpha": 0,
        "gridAlpha": 0,
        "inside": true,
        "tickLength": 0
    },
    "exportConfig": {
        "menuTop": "20px",
        "menuRight": "20px",
        "menuItems": [{
            "icon": 'http://www.amcharts.com/lib/3/images/export.png',
            "format": 'png'
        }]
    }
});
</script>
  <h1>Currency rate</h1>
  <form action="" method="POST">
    <table>
      <tr>
        <td>
          Currency
        </td>
        <td>
          <select name="currency">
<?
    foreach( $currencies as $code => $name ){
       $selected = ($currency == $code) ?  " selected" : "";
       echo '<option value="' . $code . '"' . $selected . '>' . $name . "</option>\n";
    }
?>
          </select>
        </td>
      </tr>
      <tr>
        <td>
          Month
        </td>
        <td>
          <select name="month">
<?
    foreach( $months as $code => $name ){
       $selected = ($month == $code) ?  " selected" : "";
       echo '<option value="' . $code . '"' . $selected . '>' . $name . "</option>\n";
    }
?>
          </select>
          Year
          <select name="year">
<?
    foreach( $years as $item ){
       $selected = ($year == $item) ?  " selected" : "";
       echo '<option value="' . $item . '"' . $selected . '>' . $item . "</option>\n";
    }
?>
          </select>
        </td>
      </tr>
      <tr>
        <td>
          <input type="submit" value="Show rate">
        </td>
      </tr>
    </table>
    <br>
  </form>
<?
    if( isset( $result ) ){
        if( $result ){
            echo '<div><table class="data">';
            foreach( $result as $res ){
                echo '<tr class="data"><td class="data">' . date( 'd.m.Y' , strtotime( $res['date'] ) ) . '</td><td>' . $res['value'] . '</td></tr>';
            }
            echo '</table></div><br>';
            echo '<div id="chartdiv" class ="chartdiv"></div>';
        } else {
            echo $cb->error;
        }
    }
?>

</body>
</html>